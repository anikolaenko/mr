# Setup MapReduce cluster

## Minimum requirements
 
To setup cluster you need at last 2 machines:

    * Master with Ubuntu 14.04 or newer
    * Workers (1 or more) with Ubuntu 14.04 or newer


## Installation steps


### 1. Master initial configuration

In order to start installation process you need to setup and configure Master instance. Later it will be used for
 Workers management operations.

By default new instances comes with root password or SSH public key which can be used for root access. Because of this 
installation process can has some differences.

For Master instance with IP 72.14.178.40 and root password access:

``` 
ssh root@72.14.178.40
```

with root SSH key access:

``` 
ssh -i root_access_key.pub root@72.14.178.40
```

After successful login Master machine need to be configured. This can be done via installation script
 from this repository. Run following commands for download Git repository:
 
``` 
apt-get install -y git mc
git clone https://anikolaenko@bitbucket.org/anikolaenko/mr.git
cd mr/installation/
```

In file `property.config` variable `DEFAULT_REDIS_AUTH_KEY` needs to be configured:
``` 
mcedit property.config
```
Make sure to use some secure hash as `DEFAULT_REDIS_AUTH_KEY` at least 15-30 characters.
Hit F2 and Enter to save your changes.

Variable `DEFAULT_USERNAME` can be left unchanged and default cluster user will be user with name "cluster".

Now it's time to setup Master instance: all needed software, packages and create cluster user as well:
``` 
./install-software-list.sh
```

This will take some time, about 2-3 minutes.


### 2. Workers configuration

To continue Workers configuration you need to do few things:
 
 
#### 2.1 Define list of cluster workers in file `workers.txt`:
``` 
mcedit workers.txt
```

For example:
```
72.14.185.47
72.14.185.48
```
Hit F2 and Enter to save your changes.


#### 2.2 Setup initial access from Master machine to Workers. This can be done in few steps.
 
If you have root SSH key you can just copy public and private keys in .ssh/ folder with names `root_id_rsa` and `root_id_rsa.pub`. In case if 
you have root password for Workers machines, please do the following steps:

Generate `root_id_rsa` key:
```
ssh-keygen -t rsa -f .ssh/root_id_rsa -N ""
```

Copy root SSH key to each Worker.
```
ssh-copy-id -i .ssh/root_id_rsa root@72.14.185.47
```


#### 2.3 Configure Workers

Workers configuration can be started with:
``` 
./workers-setup.sh
```

This will take some time, about 2-3 minutes per each Worker.

To test Workers configuration you can login as cluster user and try to connect to each Worker by SSH.

``` 
su - cluster
ssh WORKER_IP_2
```

#### 2.4 Worker Helper Scripts

In case if some command need to be executed on each Worker, you can use this script: 
``` 
./workers-command.sh
```

### 3. Secure configuration

Cluster code makes use of RPyC which runs on ports 23456 and 45678. If you do not block
 access to those ports from the public internet, your machines can be accessed and arbitrary
  code can be executed.

With following iptables rules you can allow access for only some IP's or sub-network:
```
sudo iptables -A INPUT -p tcp --dport 23456 -s 192.168.1.100 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 23456 -s 192.168.1.0/24 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 23456 -j DROP

sudo iptables -A INPUT -p tcp --dport 45678 -s 192.168.1.100 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 45678 -s 192.168.1.0/24 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 45678 -j DROP
```

Script `./workers-command.sh` might be helpful for add these rules on cluster Workers.