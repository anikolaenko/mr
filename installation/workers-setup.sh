#!/bin/bash

# Exit on errors:
set -e

# Include common script
SCRIPT_DIR=$(dirname $0)
source ${SCRIPT_DIR}/common.sh
source ${SCRIPT_DIR}/property.config

# Create SSh key
checkForSSHKey

printInfo "Setup workers:"

while IFS='' read -r line || [[ -n "$line" ]]; do
    ip=${line}
    printInfo "Setup packages on $ip"
    ssh -i .ssh/root_id_rsa root@${ip} 'bash -s' < <(cat ${SCRIPT_DIR}/property.config install-software-list.sh)

    # Copy cluster key to worker and allow access for it
    cat .ssh/id_rsa.pub | ssh -i .ssh/root_id_rsa root@${ip} "mkdir -p /home/${DEFAULT_USERNAME}/.ssh; cat >> /home/${DEFAULT_USERNAME}/.ssh/authorized_keys"
    scp -i .ssh/root_id_rsa -r .ssh/id_rsa* root@${ip}:/home/${DEFAULT_USERNAME}/.ssh
    ssh -i .ssh/root_id_rsa root@${ip} "chown -R ${DEFAULT_USERNAME}:${DEFAULT_USERNAME} /home/${DEFAULT_USERNAME}/.ssh" </dev/null
    ssh -i .ssh/root_id_rsa root@${ip} "mkdir /mnt/ssd0; chown -R ${DEFAULT_USERNAME}:${DEFAULT_USERNAME} /mnt/ssd0" </dev/null
done < "workers.txt"
