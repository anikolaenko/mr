#!/bin/bash

# Exit on errors:
set -e

# Get script directory:
SCRIPT_DIR=$(dirname $0)

# Load common methods:
source ${SCRIPT_DIR}/common.sh

# Create SSh key
printInfo "Write command here to execute on all servers, followed by [ENTER]:"
read command

while IFS='' read -r line || [[ -n "$line" ]]; do
    ip=${line}
    echo ""
    printInfo "Login to server: $line";
    printInfo "Execute command: $command";
    ssh -n -i .ssh/root_id_rsa root@${ip} "$command || echo" < /dev/null
done < "workers.txt"
